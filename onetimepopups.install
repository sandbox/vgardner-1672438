<?php
/**
 * @file
 * Install, update, and uninstall functions for the One Time Pop-ups module.
 * @authors
 * Vicente Gardner (vgardner) <http://vgardner.co.uk>
 */

/**
 * Implements hook_install().
 */
function onetimepopups_install() {
  drupal_install_schema('onetimepopups');
  $t = get_t();
  $translated = $t('translate this');
  drupal_set_message($t('!module has been installed.', array('!module' => l($t('One Time Popups'), 'admin/settings/onetimepopups'))));
}

/**
 * Implements hook_uninstall().
 */
function onetimepopups_uninstall() {
  drupal_uninstall_schema('onetimepopups');
  variable_del('onetimepopups_counter');
}

/**
 * Implements hook_schema().
 */
function onetimepopups_schema() {
  // Schema for search_autocomplete database.
  $schema['onetimepopups'] = array(
    'description' => 'Store the one time popups configurations.',
    'fields' => array(
      'fid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'Human readable name for the form',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => 'Type of popup',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'selector' => array(
        'description' => 'Reference id selector of the the form in drupal',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'selector' => array(
        'description' => 'Reference id selector of the the form in drupal',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'ajax_url' => array(
        'description' => 'Ajax URL or path',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'ajax_loading' => array(
        'description' => 'Path for Ajax Popup',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'default' => '',
      ),
      'body' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'description' => 'Content for HTML popup.',
      ),
      'weight' => array(
        'description' => 'Weight of popup',
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ),
      'enabled' => array(
        'description' => 'Define if popup is activated or not',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'repetition' => array(
        'description' => 'Number of times to show popup to same user',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'cookie_expiration' => array(
        'description' => 'Cookie expiration',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => 0,
      ),
      'debug' => array(
        'description' => 'Debug mode',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'delay' => array(
        'description' => 'Pop-up Delay',
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ),
      'theme' => array(
        'description' => 'Pop-up Theme',
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ),
      'visibility' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'Flag to indicate how to show blocks on pages. (0 = Show on all pages except listed pages, 1 = Show only on listed pages, 2 = Use custom PHP code to determine visibility)',
      ),
      'pages' => array(
        'type' => 'text',
        'not null' => TRUE,
        'description' => 'Contents of the "Pages" block; contains either a list of paths on which to include/exclude the block or PHP code, depending on "visibility" setting.',
      ),
      'format' => array(
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
        'default' => 0,
        'description' => "Block body's {filter_formats}.format; for example, 1 = Filtered HTML.",
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the popup was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'changed' => array(
        'description' => 'The Unix timestamp when the popup was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
    ),
    'primary key' => array('fid'),
  );
  $schema['onetimepopups_roles'] = array(
    'description' => 'Store the one time popups roles configurations.',
    'fields' => array(
      'fid' => array(
        'description' => 'Id of popup',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'rid' => array(
        'description' => 'Role Id',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
    ),
    'primary key' => array(
      'fid',
      'rid'),
    'indexes' => array(
      'rid' => array('rid'),
    ),
  );
  return $schema;
}
