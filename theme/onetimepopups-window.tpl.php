<?php
/**
 * @file
 * onetimepopups-window.tpl.php
 * Theme one time pop-up oeverlay windows
 *
 * - $window_body : Body to be displayed in overlay window
 * - $popup_theme : Theme of pop-up
 * @ingroup onetimepopups_templates
 */
?>
<div id="onetimepopups-window" class="jqmWindow popup-theme-<?php print $popup_theme; ?>">
  <a href="#" class="jqmClose"><em>Close</em></a>
  <div class="jqmContent">
  <?php print $window_body; ?>
  </div>
</div>
