<?php
/**
 * @file
 * One Time Pop-ups
 * Add a new pop-up to One Time Pop-ups list.
 *
 * @authors
 * Vicente Gardner (vgardner) <http://vgardner.co.uk>
 *
 */

/**
 * MENU CALLBACK:
 * Define the page to add a form.
 * @return  A rendered form
 */
function onetimepopups_form_add() {
  $form = array();
  /* ------------------------------------------------------------------ */
  $form['title'] = array(
    '#title' => t('Title'),
    '#description' => 'Please enter a title for this popup',
    '#type' => 'textfield',
    '#default_value' => '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $descr = t('Frequency of pop-up display in days. (Number of days that cookie will last)');
  $form['cookie_expiration'] = array(
    '#title' => t('Pop-up frequency (in days)'),
    '#description' => $descr,
    '#type' => 'textfield',
    '#default_value' => '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show on every page load. (Debug mode)'),
    '#description' => t('Cookie will never be set, pop-up will show on every page load. Mostly used for development and debugging.'),
  );
  $descr = t('Time for pop-up to display after the page is loaded (In milliseconds).');
  $form['delay'] = array(
    '#title' => t('Pop-up delay (in milliseconds)'),
    '#description' => $descr,
    '#type' => 'textfield',
    '#default_value' => '0',
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $descr = t('Content to be displayed');
  $form['body'] = array(
    '#title' => t('Body'),
    '#description' => $descr,
    '#type' => 'textarea',
    '#default_value' => '',
    '#required' => FALSE,
  );
  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('Pop-up theme'),
    '#options' => array(
    0 => t('Standard'),
    1 => t('Notepad'),
    2 => t('Dark'),
    ),
    '#default_value' => 0,
    '#description' => t('Set pop-up theme.'),
  );
  //AJAX settings
  $form['ajax_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ajax settings (only for popup-ups with Ajax content)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('If the Ajax path below is set, the body field will be disconsidered.')
  );
  $form['ajax_settings']['ajax_url'] = array(
    '#title' => t('Ajax path'),
    '#description' => t('Please enter the path to be displayed on the pop-up via Ajax. Only internal paths are allowed. e.g. /node/1 and not http://www.google.com'),
    '#type' => 'textfield',
    '#default_value' => '',
    '#maxlength' => 255,
    '#required' => FALSE,
  );
  $form['ajax_settings']['ajax_loading'] = array(
    '#title' => t('Loading message'),
    '#description' => t('Loading message to be displayed while AJAX loads. Allows HTML tags.'),
    '#type' => 'textarea',
    '#default_value' => '',
    '#required' => FALSE,
  );
   // Role-based visibility settings
  $default_role_options = array();
  $result = db_query("SELECT rid FROM {onetimepopups_roles}");
  while ($role = db_fetch_object($result)) {
    $default_role_options[] = $role->rid;
  }
  $result = db_query('SELECT rid, name FROM {role} ORDER BY name');
  $role_options = array();
  while ($role = db_fetch_object($result)) {
    $role_options[$role->rid] = $role->name;
  }
  $form['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role specific visibility settings'),
    '#collapsible' => TRUE,
  );
  $form['role_vis_settings']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show block for specific roles'),
    '#default_value' => $default_role_options,
    '#options' => $role_options,
    '#description' => t('Show this block only for the selected role(s). If you select no roles, the block will be visible to all users.'),
  );
  //Visibility
  $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific visibility settings'),
    '#collapsible' => TRUE,
  );
  $access = user_access('use PHP for pop-up visibility');
  $options = array(t('Show on every page except the listed pages.'), t('Show on only the listed pages.'));
  $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  //Check if user has access to PHP visibility settings
  if ($access) {
  $options[] = t('Show if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
  $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
  }
  $form['page_vis_settings']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show pop-up on specific pages'),
    '#options' => $options,
  );
  $form['page_vis_settings']['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => $description,
  );
  // submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['#validate'] = array(
    'onetimepopups_form_add_validate'
  );
  return $form;
}
// -------------------------------------------------------------------------------------
/**
 * Implementation of hook_validate().
 * Validate form
 */
function onetimepopups_form_add_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['cookie_expiration'])) {
    form_set_error('cookie_expiration', t('Pop-up frequency must be a valid number.'));
  }
  if (!is_numeric($form_state['values']['delay'])) {
    form_set_error('delay', t('Pop-up delay must be a valid number (time in milliseconds).'));
  }
}
/**
 * Implementation of hook_submit().
 * Save the new form in database
 */
function onetimepopups_form_add_submit($form, &$form_state) {
  $ok_query = TRUE;

  if (!form_get_errors()) {
    $form_state['values']['created'] = time();
    $form_state['values']['enabled'] = 1;
    $roles = $form_state['values']['roles'];
    unset($form_state['values']['roles']);
    //$form_state['values']['roles'] = serialize($form_state['values']['roles']);
    //db_query("INSERT INTO {onetimepopups} (title, selector, cookie_expiration, body, roles, visibility, pages, created, enabled, debug) VALUES ('%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s')", trim($form_state['values']['title']), trim($form_state['values']['selector']), $form_state['values']['cookie_expiration'], $form_state['values']['body'], serialize($form_state['values']['roles']), $form_state['values']['visibility'], $form_state['values']['pages'], time(), 1, $form_state['values']['debug']);
    drupal_write_record('onetimepopups', $form_state['values']);
    //exit(print_r($roles));
    foreach (array_filter($roles) as $k => $rid) {
      $role_array = array('fid' => $form_state['values']['fid'], 'rid' => $rid);
      drupal_write_record('onetimepopups_roles', $role_array);
    }
    $ok_query ? drupal_set_message(t('The popup has been created successfully !') . '<br/>' . t('Please check its configuration.')) : drupal_set_message(t("An error has occured while creating the form. Please, double check your settings!"), 'error');
    // redirect to configuration page
    $form_state['redirect'] = 'admin/settings/onetimepopups';
  return;
  }
  // Give a return to the user
}