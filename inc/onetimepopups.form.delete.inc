<?php
/**
 * @file
 * One Time Pop-Ups
 * Delete one time pop-ups.
 *
 * @authors
 * Vicente Gardner <http://vgardner.co.uk>
 */

/**
 * Return the filter delete pop-up.
 */
function onetimepopups_form_delete($form_state) {
  $form = array();

  // get data from database
  $fid = arg(3);
  $result_forms   = db_query('SELECT * FROM {onetimepopups} WHERE fid = ' . $fid);
  $item       = db_fetch_array($result_forms);

  if (!$fid) {
    drupal_set_message(
      t('The pop-up has not been found, or the menu callback received a wrong parameter.'),
      'error'
    );
    watchdog(
      'onetimepopups',
      'The form has not been found, or the menu callback received a wrong parameter.',
      NULL,
      WATCHDOG_ERROR
    );
    return $form;
  }

  $form['fid'] = array(
    '#type' => 'hidden',
    '#value' => $fid
  );
  $form['title'] = array(
    '#type' => 'hidden',
    '#value' => $item['title']
  );

  return confirm_form(
    $form,
    t("Are you sure you want to delete the '%title' pop-up?", array('%title' => $item['title'])),
    'admin/settings/search_autocomplete',
    NULL,
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submission callback for the filter delete form.
 */
function onetimepopups_form_delete_submit($form, &$form_state) {
  $ok = TRUE;
  $values = $form_state['values'];
  $fid = $values['fid'];

  $ok .= db_query('DELETE FROM {onetimepopups} WHERE fid = %d', $fid);
  $ok .= db_query('DELETE FROM {onetimepopups_roles} WHERE fid = %d', $fid);
  // Give a return to the user
  $ok ? drupal_set_message(t("'%title' has been successfully deleted !", array('%title' => $values['title']))) : drupal_set_message(t("An error has occured while deleting the form!"), 'error');

  $form_state['redirect'] = 'admin/settings/onetimepopups';
}