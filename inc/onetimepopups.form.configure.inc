<?php
/**
 * @file
 * One Time Pop-ups
 * Edit form for One Time Pop-ups.
 *
 * @authors
 * Vicente Gardner (vgardner) <http://vgardner.co.uk>
 *
 */

/**
 * MENU CALLBACK:
 * Define the form to configure the one time pop-ups.
 * @return  A rendered form
 */
function onetimepopups_form_configure() {
  $form = array();
  $base = "admin/settings/onetimepopups";  // base URL for this module configurations
  // get data from database
  $fid = arg(3);
  $result_forms   = db_query('SELECT * FROM {onetimepopups} WHERE fid = ' . $fid);
  $item   = db_fetch_array($result_forms);
  $form = array();
  $form['fid'] = array(
    '#type' => 'hidden',
    '#value' => $fid,
  );
  /* ------------------------------------------------------------------ */
  $form['title'] = array(
    '#title' => t('Title'),
    '#description' => 'Please enter a title for this popup',
    '#type' => 'textfield',
    '#default_value' => $item['title'],
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $descr = t('Frequency of pop-up display in days. (Number of days that cookie will last)');
  $form['cookie_expiration'] = array(
    '#title' => t('Pop-up frequency (in days)'),
    '#description' => $descr,
    '#type' => 'textfield',
    '#default_value' => $item['cookie_expiration'],
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show on every page load. (Debug mode)'),
    '#default_value' => $item['debug'],
    '#description' => t('If this is checked the cookie will never be set, pop-up will show on every page load. Mostly used for development and debugging.'),
  );
  $descr = t('Time for pop-up to display after the page is loaded (In milliseconds).');
  $form['delay'] = array(
    '#title' => t('Pop-up delay (in milliseconds)'),
    '#description' => $descr,
    '#type' => 'textfield',
    '#default_value' => $item['delay'],
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $descr = t('Content to be displayed');
  $form['body'] = array(
    '#title' => t('Body'),
    '#description' => $descr,
    '#type' => 'textarea',
    '#default_value' => $item['body'],
    '#required' => FALSE,
  );
  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('Pop-up theme'),
    '#options' => array(
    0 => t('Standard'),
    1 => t('Notepad'),
    2 => t('Dark'),
    ),
    '#default_value' => $item['theme'],
    '#description' => t('Set pop-up theme.'),
  );
  //AJAX settings
  $ajax_collapsed = TRUE;
  if (!empty($item['ajax_url'])) {
    $ajax_collapsed = FALSE;
  }
  $form['ajax_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ajax settings (only for popup-ups with Ajax content)'),
    '#collapsible' => TRUE,
    '#collapsed' => $ajax_collapsed,
    '#description' => t('If the Ajax path below is set, the body field will be disconsidered.')
  );
  $form['ajax_settings']['ajax_url'] = array(
    '#title' => t('Ajax path'),
    '#description' => t('Please enter the path to be displayed on the pop-up via Ajax. Only internal paths are allowed. e.g. /node/1 and not http://www.google.com'),
    '#type' => 'textfield',
    '#default_value' => $item['ajax_url'],
    '#maxlength' => 255,
    '#required' => FALSE,
  );
  $form['ajax_settings']['ajax_loading'] = array(
    '#title' => t('Loading message'),
    '#description' => t('Loading message to be displayed while AJAX loads. Allows HTML tags.'),
    '#type' => 'textarea',
    '#default_value' => $item['ajax_loading'],
    '#required' => FALSE,
  );
   // Role-based visibility settings
  $default_role_options = array();
  $result = db_query("SELECT rid FROM {onetimepopups_roles} WHERE fid = '%s'", $fid);
  while ($role = db_fetch_object($result)) {
    $default_role_options[] = $role->rid;
  }
  $result = db_query('SELECT rid, name FROM {role} ORDER BY name');
  $role_options = array();
  while ($role = db_fetch_object($result)) {
    $role_options[$role->rid] = $role->name;
  }
  $form['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role specific visibility settings'),
    '#collapsible' => TRUE,
  );
  $form['role_vis_settings']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show block for specific roles'),
    '#default_value' => $default_role_options,
    '#options' => $role_options,
    '#description' => t('Show this block only for the selected role(s). If you select no roles, the block will be visible to all users.'),
  );
  //Visibility
  $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific visibility settings'),
    '#collapsible' => TRUE,
  );
  $access = user_access('use PHP for pop-up visibility');
  $options = array(t('Show on every page except the listed pages.'), t('Show on only the listed pages.'));
  $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  if ($access) {
    $options[] = t('Show if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
    $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
  }
  $form['page_vis_settings']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show pop-up on specific pages'),
    '#options' => $options,
    '#default_value' => $item['visibility'],
  );
  $form['page_vis_settings']['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => $item['pages'],
    '#description' => $description,
  );
  // submit buton
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#validate'] = array(
  'onetimepopups_form_configure_validate'
  );
  return $form;
}
// -------------------------------------------------------------------------------------
/**
 * Implementation of hook_submit().
 * Save the changes in the database
 */
function onetimepopups_form_configure_submit($form, &$form_state) {
  $ok_query = TRUE;  // so far so good!
  //Update the database with the new values
  $what = '';
  $sids = '';
  $weights = '';
  //Update pop-up
  $values = $form_state['values'];
  $values['changed'] = time();
  $roles = $values['roles'];
  drupal_write_record('onetimepopups', $values, "fid");
  db_query('DELETE FROM {onetimepopups_roles} WHERE fid = %d', $values['fid']);
  foreach (array_filter($roles) as $k => $rid) {
    $role_array = array('fid' => $form_state['values']['fid'], 'rid' => $rid);
    drupal_write_record('onetimepopups_roles', $role_array);
  }
  $form_state['redirect'] = 'admin/settings/onetimepopups';
  $ok_query ? drupal_set_message(t("The '%title' pop-up was sucessfully updated!", array('%title' => $values['title']))) : drupal_set_message(t("An error has occured while saving the settings. Please, double check your settings!"), 'error');
}
// -------------------------------------------------------------------------------------
/**
 * CALLBACK:
 * Theme function for this treelist form
 */
function theme_onetimepopups_form_configuration_fieldset($form) {

  $header = array('Title', 'Enabled', 'Prefix of the item in suggestions', 'Weight', 'Operation');

  // for each elements to anchor in the form
  foreach (element_children($form) as $key) {
  $element = &$form[$key];
  $element['sug_weight']['#attributes']['class'] = 'weight-group';

  $row = array();
  $row[] = drupal_render($element['sug_markup']);
  $row[] = drupal_render($element['sug_enabled']);
  $row[] = drupal_render($element['sug_prefix']);
  $row[] = drupal_render($element['sug_weight']);
  $row[] = drupal_render($element['sug_edit']);

  // Add a draggable class to every table row (<tr>)
  $rows[] = array('data' => $row, 'class' => 'draggable');
  }

  // Themize the table and render the form
  $output = theme('table', $header, $rows, array('id' => 'draggable-table'));
  $output .= drupal_render($form);

  drupal_add_tabledrag('draggable-table', 'order', 'sibling', 'weight-group');

  return $output;

} // function theme_search_autocomplete_form_configuration()
// -------------------------------------------------------------------------------------
/**
 * Implementation of hook_validate().
 * Validate form
 */
function onetimepopups_form_configure_validate($form, &$form_state) {
if (!is_numeric($form_state['values']['cookie_expiration'])) {
    form_set_error('cookie_expiration', t('Pop-up frequency must be a valid number.'));
  }
  if (!is_numeric($form_state['values']['delay'])) {
    form_set_error('delay', t('Pop-up delay must be a valid number (time in milliseconds).'));
  }
}