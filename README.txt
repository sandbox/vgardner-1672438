One Time Pop-ups
http://drupal.org/project/one_time_pop_ups
====================================

DESCRIPTION
-----------
One Time Pop-ups is a module that allows admins to create and manage multiple overlay pop-ups that will only be displayed once
in a configurable frequency of time. This is useful for one time messages or notices, advertisements, follow boxes
for Facebook and other social networks.

FEATURES
-----------
- Create and manage multiple overlay pop-ups
Configurable:
- Pop-up display frequency in days
- Time for pop-up to be displayed after the page is loaded (in milliseconds)
- Pop-up content using HTML
- Ajax content from an internal path
- Pop-up style and theme
- Role specific visibility settings
- Page specific visibility settings
- Themable layout using tpl files

INSTALLATION
-----------
1) Copy imce directory to your modules directory
2) Enable the module at: /admin/build/modules
3) Create a pop-up at /admin/settings/onetimepopups
4) Test it at the configured page according to role and page visibility settings.
Notes:
 - Only one pop-up will be displayed per page. If two pop-ups are configured to be displayed on one page,
the pop-up with the lighter weight will be displayed. Adjust weight by dragging the pop-up items at /admin/settings/onetimepopups
and save.


FREQUENTLY FACED ISSUES
-----------
- Ajax content not appearing:
*Path has to be an internal path and not a URL. (e.g. /node/1 and not http://www.google.com) 
*Please make sure that content from path is accessible to the role accessing it. Check user permissions.

-Double pop-ups not appearing:
*Only one pop-up will be displayed per page. If two pop-ups are configured to be displayed on one page,
the pop-up with the lighter weight will be displayed. Adjust weight by dragging the pop-up items at /admin/settings/onetimepopups
and save.

-Pop-up frequency not altering for old users:
*One Time Pop-ups uses cookies to know whether a certain pop-up has been displayed to the user, and after
the amount of days set for the Pop-up frequency the cookie will expire and a new pop-up will be show if still enabled.
So if you alter the pop-up frequency after the pop-up was displayed to the user, that cookie will have to expire 
before a new one can be set with a new expiration date.

-Pop-up won't be displayed after theming:
*This module uses jqModal (http://dev.iceburg.net/jquery/jqModal/) for the overlay pop-ups, so please make sure
that your new theme for the pop-up is implemented according to jqModals documentation.
*Assure that you are using the ids and classes used in the original theme #onetimepopups-window and .jqmWindow
for the parent div and .jqmClose for your close button. Your content has to be inside another div with a parent
div with the ids and classes above.


EXTERNAL LIBRARIES AND PLUGINS
-----------
jQuery cookie: https://github.com/carhartl/jquery-cookie/
jqModal (for overlays): http://dev.iceburg.net/jquery/jqModal/ 